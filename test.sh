#!/bin/bash
set -e
set -u
set -x
set -o pipefail

# Get bootstrap addresses
b1=$(tezos-client list known addresses | grep bootstrap1 | cut -d' ' -f2)
b2=$(tezos-client list known addresses | grep bootstrap2 | cut -d' ' -f2)

# Originate the contract with b1's address as admin
tezos-client originate contract AHS for bootstrap1 transferring 0 from bootstrap1 running authenticatedHashStore.tz --init '(Pair {"'${b1}'"} "")'  --burn-cap 1 --force

# Display code
tezos-client get script code for AHS

# Display storage
tezos-client get script storage for AHS

# Call with correct parameters to set the storage to an initial value
tezos-client transfer 0 from bootstrap1 to AHS --arg '(Pair None (Pair "" "test1"))' --burn-cap 1

# Try unauthorized update, should fail
if tezos-client transfer 0 from bootstrap2 to AHS --arg '(Pair None (Pair "test1" "test2"))' --burn-cap 1
then
    exit 1
fi

# Try authorized update, but from erroneous previous hash value, should fail
if tezos-client transfer 0 from bootstrap1 to AHS --arg '(Pair None (Pair "" "test1"))' --burn-cap 1
then
    exit 1
fi

# Update the admin
tezos-client transfer 0 from bootstrap1 to AHS --arg '(Pair (Some {"'${b1}'" ; "'${b2}'"}) (Pair "test1" "test1"))' --burn-cap 1

# Retry the update by b2, should work now
tezos-client transfer 0 from bootstrap2 to AHS --arg '(Pair None (Pair "test1" "test2"))' --burn-cap 1
